import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {LoginComponent} from './components/login/login.component';
import {MainComponent} from './components/main/main.component';
import {AuthGuard} from './auth.guard';
import {PriceComponent} from './components/price/price.component';
import {PersonalComponent} from './components/personal/personal.component';
import {ResponsePersonalComponent} from './components/response-personal/response-personal.component';
import {AnalyticsComponent} from './components/analytics/analytics.component';
import {AuthGuardLogin} from './auth_login.guard';
import {PersonalResolverService} from './resolvers/personal-resolver.service';
import {PersonalInfoResolverService} from './resolvers/personal-info-resolver.service';
import {UserInfoComponent} from './components/user-info/user-info.component';
import {PriceListComponent} from './components/price-list/price-list.component';
import {PriceListResolverService} from './resolvers/price-list-resolver.service';
import {AnalyticsResolverService} from './resolvers/analytics-resolver.service';
import {OrderInfoComponent} from './components/order-info/order-info.component';
import {OrderInfoResolverService} from './resolvers/order-info-resolver.service';
import {ResponcePersonalResolverService} from './resolvers/responce-personal-resolver.service';

const itemRoutes: Routes = [
  {
    runGuardsAndResolvers: 'always',
    path: 'analytics',
    component: AnalyticsComponent,
    pathMatch: 'full',
    resolve: [AnalyticsResolverService]
  },
  {
    path: 'price',
    component: PriceListComponent,
    resolve: [PriceListResolverService],
    pathMatch: 'full'
  },
  {
    path: 'personal',
    component: PersonalComponent,
    resolve: [PersonalResolverService],
    pathMatch: 'full'
  },
  {
    path: 'personal/:id',
    component: UserInfoComponent,
    resolve: [PersonalInfoResolverService],
    pathMatch: 'full'
  },
  {
    path: 'settings',
    component: ResponsePersonalComponent,
    pathMatch: 'full',
    resolve: [ResponcePersonalResolverService]
  },
  {
    path: 'analytics/:idUser/:idOrder',
    component: OrderInfoComponent,
    pathMatch: 'full',
    resolve: [OrderInfoResolverService]
  }
];

export const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard],
    redirectTo: 'main/analytics',
    pathMatch: 'full'
  },
  {path: 'login', component: LoginComponent, pathMatch: 'full', canActivate: [AuthGuardLogin]},
  {path: 'main', component: MainComponent, children: itemRoutes, canActivate: [AuthGuard]},
  {path: '**', canActivate: [AuthGuard], redirectTo: 'main/analytics'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
