import {Injectable} from '@angular/core';
import {AngularFireDatabase} from 'angularfire2/database';
import {
  Approved,
  BankAccount,
  BankCard,
  License,
  Passport,
  Personal,
  Photo,
  Profile,
  ProfileInfo,
  ProfileTable,
  Status
} from '../models/Accounts';
import DataSnapshot = firebase.database.DataSnapshot;

@Injectable({
  providedIn: 'root'
})
export class PersonalService {

  constructor(private db: AngularFireDatabase) {
  }


  getProfiles(): Promise<any> {
    return this.db.database.ref('/profile').once('value');
  }

  getApproved(): Promise<any> {
    return this.db.database.ref('/approved').once('value');
  }


  getProfileTable(data: any): ProfileTable {
    const request1 = data[0][0] as DataSnapshot;
    const request2 = data[0][1] as DataSnapshot;
    const profileTemp = {
      profiles: []
    };
    request1.forEach(item1 => {
      request2.forEach(item2 => {
        if (item2.key === item1.key) {
          const values = item2.val();
          const status = this.getStatus(values);

          profileTemp.profiles.push({
            id: item1.key,
            name: item1.val().name,
            birthday: item1.val().birthday,
            phone: item1.val().phone,
            status
          });
        }
      });
    });
    return profileTemp as ProfileTable;
  }

  getRegistrationById(id: string) {
    return this.db.database.ref('/registartion').child(id).once('value');
  }

  getApprovedById(id: string) {
    return this.db.database.ref('/approved').child(id).once('value');
  }

  getRegistrationInfo(data: any): ProfileInfo {
    const infoUser = data[0][0] as DataSnapshot;
    let email: string;
    let license: License;
    let bank: BankAccount;
    let card: BankCard;
    let passport: Passport;
    let personal: Personal;
    let photo: Photo;
    infoUser.forEach(item => {
      switch (item.key) {
        case 'email': {
          email = item.val();
          break;
        }
        case 'license': {
          license = item.val() as License;
          break;
        }
        case 'bank': {
          bank = item.val() as BankAccount;
          break;
        }
        case 'card': {
          card = item.val() as BankCard;
          break;
        }
        case 'passport': {
          passport = item.val() as Passport;
          break;
        }
        case 'personal': {
          personal = item.val() as Personal;
          break;
        }
        case 'photos': {
          photo = item.val() as Photo;
          break;
        }
      }
    });
    const profileInfo = {
      bank,
      card,
      email,
      license,
      passport,
      personal,
      photo
    };
    // @ts-ignore
    return profileInfo as ProfileInfo;
  }

  getStatus(values: any): Status {
    const approved = values.approved_to_works;
    const canceled = !values.approved_to_works && (values.bad_card_data || values.bad_bank_data || values.bad_email_data ||
      values.bad_license_back_photo || values.bad_license_data || values.bad_license_front_photo || values.bad_passport_data ||
      values.bad_passport_first_photo || values.bad_passport_reg_photo || values.bad_passport_selfie || values.bad_personal_data);
    let status = Status.WAITING;
    if (approved) {
      status = Status.ACCEPTED;
    } else if (canceled) {
      status = Status.CANCELED;
    }
    return status;
  }

  getApprovedInfo(data: any): Approved {
    const approvedUser = data[0][1] as DataSnapshot;
    const approvedUserData = {
      approved_to_works: approvedUser.val().approved_to_works,
      bad_bank_data: approvedUser.val().bad_bank_data,
      bad_card_data: approvedUser.val().bad_card_data,
      bad_email_data: approvedUser.val().bad_email_data,
      bad_license_back_photo: approvedUser.val().bad_license_back_photo,
      bad_license_data: approvedUser.val().bad_license_data,
      bad_license_front_photo: approvedUser.val().bad_license_front_photo,
      bad_passport_data: approvedUser.val().bad_passport_data,
      bad_passport_first_photo: approvedUser.val().bad_passport_first_photo,
      bad_passport_reg_photo: approvedUser.val().bad_passport_reg_photo,
      bad_passport_selfie: approvedUser.val().bad_passport_selfie,
      bad_personal_data: approvedUser.val().bad_personal_data
    };
    return approvedUserData as Approved;
  }

  setApproved(id: string, values: boolean[], approved: boolean, callback: any) {
    if (approved) {
      this.db.database.ref('/approved').child(id).set({
        approved_to_works: approved,
        bad_bank_data: false,
        bad_card_data: false,
        bad_email_data: false,
        bad_license_back_photo: false,
        bad_license_data: false,
        bad_license_front_photo: false,
        bad_passport_data: false,
        bad_passport_first_photo: false,
        bad_passport_reg_photo: false,
        bad_passport_selfie: false,
        bad_personal_data: false
      }, result => {
        console.log(result);
        callback(!result);
      });
    } else {
      this.db.database.ref('/approved').child(id).set({
        approved_to_works: approved,
        bad_bank_data: values[0],
        bad_card_data: values[0],
        bad_email_data: values[1],
        bad_license_front_photo: values[2],
        bad_license_back_photo: values[3],
        bad_license_data: values[4],
        bad_passport_data: values[5],
        bad_passport_first_photo: values[6],
        bad_passport_reg_photo: values[7],
        bad_passport_selfie: values[8],
        bad_personal_data: values[9]
      }, result => {
        console.log('FAAAAIL!!' + result);
        callback(!result);
      });
    }
  }

  getProfileById(idUser: string) {
    return this.db.database.ref('/profile').child(idUser).once('value');
  }

  getPersonalInfo(data: any): Profile {
    const info = data[0][1] as DataSnapshot;
    return info.val() as Profile;
  }
}
