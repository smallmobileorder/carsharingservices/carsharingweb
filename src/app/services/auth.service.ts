import {Injectable} from '@angular/core';

import {AngularFireAuth} from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import {Router} from '@angular/router';


@Injectable()
export class AuthService {
  user: firebase.User;

  constructor(private firebaseAuth: AngularFireAuth, private  router: Router) {

  }

  getUserState() {
    return this.firebaseAuth.authState;
  }

  login(email: string, password: string, callback: any) {
    this.firebaseAuth
      .auth
      .signInWithEmailAndPassword(email, password)
      .then(userCredential => {
        if (userCredential) {
          this.router.navigate(['main/analytics']);
          callback(true);
        }
        callback(false);
      })
      .catch(err => {
        console.log('Something went wrong:', err.message);
        callback(false);
      });
  }

  logout() {
    this.firebaseAuth
      .auth
      .signOut().then(result => {
      this.router.navigate(['/login']);
    });
  }
}
