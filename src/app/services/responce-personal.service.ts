import {Injectable} from '@angular/core';
import {AngularFireDatabase} from '@angular/fire/database';
import {Feedback, FeedbackTable} from '../models/Responce';
import DataSnapshot = firebase.database.DataSnapshot;
import {Profile} from '../models/Accounts';

@Injectable({
  providedIn: 'root'
})
export class ResponcePersonalService {

  constructor(private db: AngularFireDatabase) {
  }

  getResponces(): Promise<any> {
    return this.db.database.ref('/feedback').once('value');
  }

  getFeedbackTable(data: any): FeedbackTable[] {
    const personals = data[0][0] as DataSnapshot;
    const feedbacks = data[0][1] as DataSnapshot;
    const feedbackTable = [];
    feedbacks.forEach(idUser => {
      idUser.forEach(feedback => {
        personals.forEach(user => {
          if (user.key === idUser.key) {
            const feedbcak = feedback.val() as Feedback;
            console.log(feedbcak);
            const profile = user.val() as Profile;
            const feedbackCell = {
              date: feedbcak.date,
              text: feedbcak.text,
              photo_link: feedbcak.photo_link,
              phone: profile.phone,
              name: profile.name
            };
            const feedbCell = feedbackCell as FeedbackTable;
            feedbackTable.push(feedbCell);
          }
        });
      });
    });
    return feedbackTable;
  }
}
