import {Injectable} from '@angular/core';
import {AngularFireDatabase} from '@angular/fire/database';
import {AngularFireStorage} from '@angular/fire/storage';
import {Order, WorkOrder} from '../models/Orders';
import DataSnapshot = firebase.database.DataSnapshot;
import {FeedbackTable} from '../models/Responce';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {delay} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AnalyticsService {

  constructor(private db: AngularFireDatabase, private storage: AngularFireStorage, private httpClient: HttpClient) {
  }

  getAllOrders(): Promise<any> {
    console.log('TRIGGGGER RESOLVER');
    return this.db.database.ref('/workGroups').once('value');
  }


  getOrders(data: any): Order[] {
    const workGroups = data[0] as DataSnapshot;
    const orders = [];
    workGroups.forEach(user => {
      user.forEach(order => {
        const orderResult = order.val() as Order;
        const works = [];
        let status = true;
        let price = 0;
        order.forEach(property => {
          if (property.key === 'works') {
            property.forEach(work => {
              const workResult = work.val() as WorkOrder;
              workResult.status = !!workResult.finishTime;
              if (workResult.status) {
                const dataFinish = new Date(workResult.finishTime);
                workResult.finishTimeString = dataFinish.toLocaleDateString() + '  ' + dataFinish.toLocaleTimeString();
              }
              const dataStart1 = new Date(workResult.startTime);
              workResult.startTimeString = dataStart1.toLocaleDateString() + '  ' + dataStart1.toLocaleTimeString();
              price = price + workResult.cost;
              status = status && workResult.status;
              works.push(workResult);
            });
          }
        });
        orderResult.idParent = user.key;
        orderResult.id = order.key;
        orderResult.works = works;
        orderResult.status = status;
        if (orderResult.status && orderResult.finishTime) {
          const dataFinish = new Date(orderResult.finishTime);
          orderResult.finishTimeString = dataFinish.toLocaleDateString() + '  ' + dataFinish.toLocaleTimeString();
        }
        const dataStart = new Date(orderResult.startTime);
        orderResult.startTimeString = dataStart.toLocaleDateString() + '  ' + dataStart.toLocaleTimeString();
        orderResult.price = price;
        orderResult.numberPlateFirstLetter = orderResult.numberPlate.charAt(0);
        orderResult.numberPlateNumber = orderResult.numberPlate.substr(1, 3);
        orderResult.numberPlateLatLetter = orderResult.numberPlate.substr(4, 2);
        orderResult.numberPlateRegion = orderResult.numberPlate.substring(6, orderResult.numberPlate.length);
        orders.push(orderResult);
      });
    });
    return orders;
  }

  getOrderById(id: string, idOrder: string): Promise<any> {
    return this.db.database.ref('/workGroups/' + id + '/' + idOrder).once('value');
  }

  getOrderInfo(data: any) {
    const info = data[0][0] as DataSnapshot;
    const orderResult = info.val() as Order;
    const works = [];
    let status = true;
    let price = 0;
    info.forEach(param => {
      if (param.key === 'works') {
        param.forEach(work => {
          const workResult = work.val() as WorkOrder;
          workResult.status = !!workResult.finishTime;
          const dataStart1 = new Date(workResult.startTime);
          workResult.startTimeString = dataStart1.toLocaleDateString() + '  ' + dataStart1.toLocaleTimeString();
          if (workResult.status) {
            console.log('workResult.finishTime = ' + workResult.finishTime);
            console.log('workResult.startTime = ' + workResult.startTime);
            const dataFinish = new Date(workResult.finishTime);
            workResult.finishTimeString = dataFinish.toLocaleDateString() + '  ' + dataFinish.toLocaleTimeString();
            const timeWorkingTemp = new Date(workResult.finishTime - workResult.startTime).getTime();
            console.log('timeWorkingTemp' + timeWorkingTemp);

            const countDay = Math.floor(timeWorkingTemp / (1000 * 60 * 60 * 24));
            const hours = Math.floor((timeWorkingTemp - countDay * 1000 * 60 * 60 * 24) / (1000 * 60 * 60));
            const min = Math.floor((timeWorkingTemp - countDay * 1000 * 60 * 60 * 24 - hours * 1000 * 60 * 60) / (1000 * 60));
            workResult.timeWorkingSec = Math.floor((timeWorkingTemp - countDay * 1000 * 60 * 60 * 24 - hours * 1000 * 60 * 60 -
              min * 1000 * 60) / 1000);
            workResult.timeWorkingMins = min;
            workResult.timeWorkingHours = hours;
            workResult.timeWorkingDay = countDay;
            console.log('workResult.timeWorkingSec' + workResult.timeWorkingSec);
            console.log('workResult.timeWorkingMins' + workResult.timeWorkingMins);
            console.log('workResult.timeWorkingDay' + workResult.timeWorkingDay);
          } else {
            const timeWorkingTemp = new Date().valueOf() - workResult.startTime;
            const countDay = Math.floor(timeWorkingTemp / (1000 * 60 * 60 * 24));
            const hours = Math.floor((timeWorkingTemp - countDay * 1000 * 60 * 60 * 24) / (1000 * 60 * 60));
            const min = Math.floor((timeWorkingTemp - countDay * 1000 * 60 * 60 * 24 - hours * 1000 * 60 * 60) / (1000 * 60));
            workResult.timeWorkingSec = Math.floor((timeWorkingTemp - countDay * 1000 * 60 * 60 * 24 - hours * 1000 * 60 * 60 -
              min * 1000 * 60) / 1000);
            workResult.timeWorkingMins = min;
            workResult.timeWorkingHours = hours;
            workResult.timeWorkingDay = countDay;
          }
          workResult.id = work.key;
          price = price + workResult.cost;
          status = status && workResult.status;
          works.push(workResult);
        });
      }
    });
    orderResult.id = info.key;
    orderResult.works = works;
    orderResult.status = status;
    if (orderResult.status) {
      const dataFinish = new Date(orderResult.finishTime);
      orderResult.finishTimeString = dataFinish.toLocaleDateString() + '  ' + dataFinish.toLocaleTimeString();
    }
    const dataStart = new Date(orderResult.startTime);
    orderResult.startTimeString = dataStart.toLocaleDateString() + '  ' + dataStart.toLocaleTimeString();
    orderResult.price = price;
    orderResult.numberPlateFirstLetter = orderResult.numberPlate.charAt(0);
    orderResult.numberPlateNumber = orderResult.numberPlate.substr(1, 3);
    orderResult.numberPlateLatLetter = orderResult.numberPlate.substr(4, 2);
    orderResult.numberPlateRegion = orderResult.numberPlate.substring(6, orderResult.numberPlate.length);
    orderResult.works = works;
    return orderResult;
  }

  downloadTable() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*'
      })
    };
    this.httpClient.get('https://us-central1-carsharingservice-5fb53.cloudfunctions.net/createReport', httpOptions).subscribe(result => {
      delay(5);
      this.storage.storage.ref('report_01.2020/report.xlsx').getDownloadURL().then(it => {
        if (it) {
          this.openImage(it);
        }
      });
    }, err => {
      console.log(err);
      delay(5);
      this.storage.storage.ref('report_01.2020/report.xlsx').getDownloadURL().then(it => {
        if (it) {
          this.openImage(it);
        }
      });
    });

  }

  openImage(link: string) {
    window.open(link, '_blank');
  }
}
