import {Injectable} from '@angular/core';
import {AngularFireDatabase} from '@angular/fire/database';
import {Category, GroupWork, Services, Work} from '../models/PriceList';
import DataSnapshot = firebase.database.DataSnapshot;

@Injectable({
  providedIn: 'root'
})
export class PriceListService {

  constructor(private db: AngularFireDatabase) {
  }

  getServices(): Promise<any> {
    return this.db.database.ref('/services').once('value');
  }

  getServiceData(data: any): Services[] {
    const servicesData = data as DataSnapshot;
    const services = [];
    servicesData.forEach(service => { // _1, _2
      const serviceMask = service.val();
      const categories = [];
      service.forEach(propertyService => { // categories, name, timeStamp
        if (propertyService.key === 'categories') {
          propertyService.forEach(category => { // _1, _2, _3
            const groupWorksComplete = [];
            const worksComplete = [];
            const categoryMask = category.val();
            category.forEach(categoryProperty => {  // name, workGroups, works
              if (categoryProperty.key === 'works') {
                categoryProperty.forEach(work => {  // _1, _2
                  const workMask = work.val();
                  let availableFrom = '0';
                  if (workMask.availableFrom) {
                    availableFrom = workMask.availableFrom;
                  }
                  let availableTo = '0';
                  if (workMask.availableTo) {
                    availableTo = workMask.availableTo;
                  }
                  const workResult = {
                    id: work.key,
                    availableFrom,
                    availableTo,
                    cost: workMask.cost,
                    defaultTime: workMask.defaultTime,
                    name: workMask.name,
                    service: workMask.service
                  };
                  const workComplete = (workResult as Work);  // cost, name,defTime
                  worksComplete.push(workComplete);
                });
              }
              if (categoryProperty.key === 'workGroups') {
                categoryProperty.forEach(workGroup => { // _1, _2
                  const workGroupMask = workGroup.val();
                  const works1 = [];
                  workGroup.forEach(workGroupProperty => {  // name, works
                    if (workGroupProperty.key === 'works') {
                      workGroupProperty.forEach(work1 => {  // _1, _2
                        const work1Mask = work1.val();
                        let availableFrom = '0';
                        if (work1Mask.availableFrom) {
                          availableFrom = work1Mask.availableFrom;
                        }
                        let availableTo = '0';
                        if (work1Mask.availableTo) {
                          availableTo = work1Mask.availableTo;
                        }
                        const work1Result = {
                          id: work1.key,
                          availableFrom,
                          availableTo,
                          cost: work1Mask.cost_employer,
                          cost_work: work1Mask.cost,
                          defaultTime: work1Mask.defaultTime,
                          name: work1Mask.name,
                          service: work1Mask.service
                        };
                        const work1Complete = work1Result as Work;  // cost, name
                        works1.push(work1Complete);
                      });
                    }
                  });
                  const workGroupResult = {
                    id: workGroup.key,
                    name: workGroupMask.name,
                    works: works1
                  };
                  const groupWorkComplete = workGroupResult as GroupWork;
                  groupWorksComplete.push(groupWorkComplete);
                });
              }
            });
            const categoryResult = {
              id: category.key,
              name: categoryMask.name,
              works: worksComplete,
              groupsWork: groupWorksComplete
            };
            const categoryComplete = categoryResult as Category;
            categories.push(categoryComplete);
          });
        }
      });

      const servicesResult = {
        id: service.key,
        name: serviceMask.name,
        email: serviceMask.email,
        timestamp: serviceMask.timestamp,
        categories
      };
      const serviceComplete = servicesResult as Services;
      services.push(serviceComplete);
    });
    return services;
  }

  updateService(services: Services[], ownerEmail: string, callback: any) {
    console.log(services);
    this.db.database.ref('owner').set({});
    this.db.database.ref('/owner').set({
      email: ownerEmail
    });
    this.db.database.ref('services').set({});
    console.log('1');
    services.forEach(service => {
      console.log(service.name);
      console.log(service.id);
      console.log(Number.parseInt(service.timestamp, 10));
      console.log(service.email);
      this.db.database.ref('/services/' + service.id).set({
        name: service.name,
        timestamp: Number.parseInt(service.timestamp, 10),
        email: service.email
      });
      console.log('3');
      service.categories.forEach(category => {
        console.log('4');
        this.db.database.ref('/services/' + service.id + '/categories/' + category.id).set({
          name: category.name,
        });
        console.log('5');
        category.groupsWork.forEach(groupWork => {
          this.db.database.ref('/services/' + service.id + '/categories/' + category.id + '/workGroups/' + groupWork.id).set({
            name: groupWork.name,
          });
          groupWork.works.forEach(workInGroup => {
            this.db.database.ref('/services/' + service.id + '/categories/' + category.id + '/workGroups/' + groupWork.id + '/works/'
              + workInGroup.id).set({
              name: workInGroup.name,
              cost: Number.parseInt(workInGroup.cost_work, 10),
              cost_employer: Number.parseInt(workInGroup.cost, 10),
              defaultTime: Number.parseInt(workInGroup.defaultTime, 10),
              service: workInGroup.service,
              availableFrom: Number.parseInt(workInGroup.availableFrom, 10),
              availableTo: Number.parseInt(workInGroup.availableTo, 10),
            });
          });
        });
        console.log('6');
        category.works.forEach(work => {
          this.db.database.ref('/services/' + service.id + '/categories/' + category.id + '/works/' + work.id).set({
            name: work.name,
            cost: Number.parseInt(work.cost, 10),
            defaultTime: Number.parseInt(work.defaultTime, 10),
            service: work.service,
            availableFrom: Number.parseInt(work.availableFrom, 10),
            availableTo: Number.parseInt(work.availableTo, 10),
          });
        });
        console.log('7');
      });
      console.log('8');
    });
    callback(true);
  }

  getOwner() {
    return this.db.database.ref('/owner').once('value');
  }

  getOwnerData(dataOwner: any): string {
    const dataOwner1 = dataOwner as DataSnapshot;
    let result = 'test@test.ru';
    dataOwner1.forEach(item => {
      if (item.key === 'email') {
        const temp = item.val() as string;
        if (temp) {
          result = temp;
        }
      }
    });
    console.log(dataOwner1.val());
    return result;
  }
}

