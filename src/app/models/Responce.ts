export interface Feedback {
  date: string;
  text: string;
  photo_link: string;
}

export interface FeedbackTable {
  date: string;
  text: string;
  photo_link: string;
  phone: string;
  name: string;
}
