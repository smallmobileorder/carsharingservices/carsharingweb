export interface RegisterAccount {
  card_name: string;
  card_number: string;
  card_: string;

}

export interface ProfileTable {
  profiles: ProfileWithStatus[];
}

export interface ProfileWithStatus {
  id: string;
  birthday: string;
  name: string;
  phone: string;
  status: boolean;
}

export interface Profile {
  id: string;
  birthday: string;
  name: string;
  phone: string;
}

export enum Status {
  ACCEPTED,
  CANCELED,
  WAITING
}

export interface ProfileInfo {
  bank: BankAccount;
  card: BankCard;
  email: string;
  license: License;
  passport: Passport;
  personal: Personal;
  photo: Photo;
}

export interface License {
  end_date: string;
  number: string;
  serial: string;
  start_date: string;
}

export interface BankAccount {
  bank_name: string;
  bic: string;
  kor_bill: string;
  name: string;
  number: string;
}

export interface BankCard {
  name: string;
  number: string;
}

export interface Passport {
  date: string;
  number: string;
  orgAddress: string;
  second_serial: string;
  serial: string;
}

export interface Personal {
  date: string;
  gender: string;
  name: string;
  second_name: string;
  surname: string;
}

export interface Photo {
  license_back: string;
  license_front: string;
  passport_first_page: string;
  passport_reg_page: string;
  passport_selfie: string;
}

export interface Approved {
  approved_to_works: boolean;
  bad_bank_data: boolean;
  bad_card_data: boolean;
  bad_email_data: boolean;
  bad_license_back_photo: boolean;
  bad_license_data: boolean;
  bad_license_front_photo: boolean;
  bad_passport_data: boolean;
  bad_passport_first_photo: boolean;
  bad_passport_reg_photo: boolean;
  bad_passport_selfie: boolean;
  bad_personal_data: boolean;
}
