export interface Order {
  idParent: string;
  id: string;
  finishTime: number;
  finishTimeString: string;
  numberPlate: string;
  numberPlateFirstLetter: string;
  numberPlateNumber: string;
  numberPlateLatLetter: string;
  numberPlateRegion: string;
  serviceName: string;
  startTime: number;
  startTimeString: string;
  works: WorkOrder[];
  status: boolean;
  price: number;
}
export interface WorkOrder {
  timeWorking: string;
  timeWorkingDay: number;
  timeWorkingHours: number;
  timeWorkingMins: number;
  timeWorkingSec: number;
  id: string;
  cost: number;
  defaultTime: number;
  finishTime: number;
  finishTimeString: string;
  name: string;
  numberPlate: string;
  photoAfterLink: string;
  photoBeforeLink: string;
  service: string;
  startTime: number;
  startTimeString: string;
  status: boolean;
}
