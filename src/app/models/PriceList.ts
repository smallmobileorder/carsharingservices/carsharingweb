export interface Services extends IdModel {
  name: string;
  timestamp: string;
  email: string;
  categories: Category[];
}

export interface Category extends IdModel {
  name: string;
  works: Work[];
  groupsWork: GroupWork[];
}

export interface Work extends IdModel {
  availableFrom: string;
  availableTo: string;
  cost: string;
  cost_work: string;
  defaultTime: string;
  name: string;
  service: string;
}

export interface GroupWork extends IdModel {
  name: string;
  works: Work[];
}

export interface IdModel {
  id: string;
}
