import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {FeedbackTable} from '../../models/Responce';
import {ActivatedRoute} from '@angular/router';
import {ResponcePersonalService} from '../../services/responce-personal.service';
import {MatPaginator, MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-settings',
  templateUrl: './response-personal.component.html',
  styleUrls: ['./response-personal.component.css']
})
export class ResponsePersonalComponent implements OnInit, AfterViewInit {
  data: any;
  feedbackTable: FeedbackTable[];
  feedbackTableTemp: FeedbackTable[];
  length = 0;
  displayedColumns: string[] = ['date', 'name', 'phone', 'text', 'img'];
  dataSource: any;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;


  constructor(private responsePersonalService: ResponcePersonalService, private route: ActivatedRoute) {
    this.data = this.route.snapshot.data;
  }

  ngOnInit() {
    this.feedbackTable = this.responsePersonalService.getFeedbackTable(this.data).reverse();
    this.dataSource = new MatTableDataSource<FeedbackTable>(this.feedbackTable);
    this.dataSource.paginator = this.paginator;
    this.length = this.feedbackTable.length;

    this.feedbackTableTemp = this.feedbackTable.slice(this.paginator.pageIndex * 20, (this.paginator.pageIndex + 1) * 20);
  }

  openImage(element: FeedbackTable) {
    window.open(element.photo_link, '_blank');
  }

  ngAfterViewInit(): void {
    this.initPageListener();
  }

  initPageListener() {
    this.paginator.page.subscribe(() => {
      this.feedbackTableTemp = this.feedbackTable.slice(this.paginator.pageIndex * 20, (this.paginator.pageIndex + 1) * 20);
    });
  }
}
