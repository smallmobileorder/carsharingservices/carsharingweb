import {Component, OnInit, ViewChild} from '@angular/core';
import {ProfileTable, ProfileWithStatus, RegisterAccount} from '../../models/Accounts';
import {PersonalService} from '../../services/personal.service';
import {ActivatedRoute, Router} from '@angular/router';
import {MatPaginator, MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-personal',
  templateUrl: './personal.component.html',
  styleUrls: ['./personal.component.css']
})
export class PersonalComponent implements OnInit {
  private data: any;

  private profileTable: ProfileTable;
  private profiles: ProfileWithStatus[];
  dataSource: any;
  displayedColumns: string[] = ['fio', 'birthday', 'phone', 'status'];

  constructor(private personalService: PersonalService, private route: ActivatedRoute, private router: Router) {
    this.data = this.route.snapshot.data;
  }

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  length = 0;

  ngOnInit() {
    this.profileTable = this.personalService.getProfileTable(this.data);
    this.profiles = this.profileTable.profiles;
    this.dataSource = new MatTableDataSource<ProfileWithStatus>(this.profiles);
    this.dataSource.paginator = this.paginator;
    this.length = Math.ceil(this.profiles.length / 20);
  }

  selectUser(id: string) {
    this.router.navigate(['/main/personal/' + id]);
  }
}
