import { Component} from '@angular/core';

@Component({
  selector: 'app-bar-chart',
  templateUrl: './barChart.component.svg',
  styleUrls: ['./barChart.component.css']
})
export class BarChartComponent {
  fillColor = 'rgb(255, 0, 0)';

  changeColor() {
    const r = Math.floor(Math.random() * 256);
    const g = Math.floor(Math.random() * 256);
    const b = Math.floor(Math.random() * 256);
    this.fillColor = `rgb(${r}, ${g}, ${b})`;
  }
}


