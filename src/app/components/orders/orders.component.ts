import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AnalyticsService} from '../../services/analytics.service';
import {Order} from '../../models/Orders';
import {MatPaginator} from '@angular/material';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit, AfterViewInit {
  private data: any;
  length = 0;
  orders: Order[];
  ordersTemp: Order[];
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;

  constructor(private analyticsService: AnalyticsService, private route: ActivatedRoute, private router: Router) {
    this.data = this.route.snapshot.data;
    console.log(this.data);
  }

  ngOnInit(): void {
    this.orders = this.analyticsService.getOrders(this.data);
    this.length = this.orders.length;
    this.orders = this.orders.sort((a, b) => b.startTime - a.startTime);
    console.log('NG_ON_INIT');
  }

  ngAfterViewInit(): void {
    console.log('NG_AFTER_INIT');
    this.ordersTemp = this.orders.slice(this.paginator.pageIndex * 20, (this.paginator.pageIndex + 1) * 20);
    console.log(this.ordersTemp);
    this.initPageListener();
    this.router.navigateByUrl('/main/analytics', {skipLocationChange: true}).then(() => {
      this.router.navigate(['/main/analytics']);
    });
  }

  initPageListener() {
    this.paginator.page.subscribe(() => {
      this.ordersTemp = this.orders.slice(this.paginator.pageIndex * 20, (this.paginator.pageIndex + 1) * 20);
    });
  }

  openOrder(order: Order) {
    this.router.navigate(['main/analytics/' + order.idParent + '/' + order.id]);
  }

  download() {
    this.analyticsService.downloadTable();
  }
}
