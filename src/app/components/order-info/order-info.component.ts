import {Component, OnInit} from '@angular/core';
import {AnalyticsService} from '../../services/analytics.service';
import {ActivatedRoute, Router} from '@angular/router';
import {PersonalService} from '../../services/personal.service';
import {Order, WorkOrder} from '../../models/Orders';
import {Profile, ProfileWithStatus} from '../../models/Accounts';
import {MatTableDataSource} from '@angular/material';
import {FeedbackTable} from '../../models/Responce';

@Component({
  selector: 'app-order-info',
  templateUrl: './order-info.component.html',
  styleUrls: ['./order-info.component.css']
})
export class OrderInfoComponent implements OnInit {
  private data: any;
  public orderInfo: Order;
  public profile: Profile;
  dataSource: any;
  displayedColumns: string[] = ['name', 'startTimeString', 'finishTimeString', 'status', 'cost'];
  selectedWork: WorkOrder;


  constructor(private analyticsService: AnalyticsService, private personalService: PersonalService, private route: ActivatedRoute,
              private router: Router) {
    this.data = this.route.snapshot.data;
  }

  ngOnInit() {
    this.profile = this.personalService.getPersonalInfo(this.data);
    this.orderInfo = this.analyticsService.getOrderInfo(this.data);
    this.dataSource = new MatTableDataSource<WorkOrder>(this.orderInfo.works);
    this.selectedWork = this.orderInfo.works[0];
  }

  setItem(row: WorkOrder) {
    this.selectedWork = row;
  }

  openImage(link: string) {
    window.open(link, '_blank');
  }
}
