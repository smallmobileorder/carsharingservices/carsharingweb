import {Component, Inject, OnInit} from '@angular/core';
import {PriceListService} from '../../services/price-list.service';
import {ActivatedRoute} from '@angular/router';
import {Category, GroupWork, IdModel, Services, Work} from '../../models/PriceList';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-price-list',
  templateUrl: './price-list.component.html',
  styleUrls: ['./price-list.component.css']
})
export class PriceListComponent implements OnInit {
  services: Services[];
  private readonly data1: any;
  name = '';
  price_max = '';
  price_min = '';
  defaultTime = '';
  availableFrom = '';
  availableTo = '';
  state = 0;
  public isChange = false;
  emailForm = new FormControl('', [Validators.email]);
  emailForm1 = new FormControl('', [Validators.email]);
  ownerEmail = ' ';
  private ownerEmailSend = '';

  constructor(private priceListService: PriceListService, private route: ActivatedRoute, public dialog: MatDialog) {
    this.data1 = this.route.snapshot.data;
  }

  ngOnInit() {
    console.log(this.data1);
    this.services = this.priceListService.getServiceData(this.data1[0][0]);
    this.ownerEmail = this.priceListService.getOwnerData(this.data1[0][1]);
    this.ownerEmailSend = this.ownerEmail;
  }

  checkChangeOwnerEmail(email: string) {
    if (this.emailForm.invalid) {
      this.isChange = false;
    } else {
      this.isChange = true;
      this.ownerEmailSend = this.emailForm1.value
    }
  }

  checkChange(service: Services) {
    if (this.emailForm.invalid) {
      this.isChange = false;
    } else {
      this.isChange = true;
      this.services[this.services.indexOf(service)].email = this.emailForm.value;
    }
  }

  openDialog(state: number, idService?: string, idCategory?: string, idGroupWork?: string) {
    this.state = state;
    const dialogRef = this.dialog.open(DialogAddDialogComponent, {
      width: '400px',
      data: {
        state, name: this.name, price_max: this.price_max, price_min: this.price_min, defaultTime: this.defaultTime, availableFrom: this.availableFrom,
        availableTo: this.availableTo
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('state = ' + state);
      if (result) {
        this.name = result;
        if (state === 3 || state === 5) {
          this.name = result.name;
          this.price_max = result.price_max;
          this.price_min = result.price_min;
          this.defaultTime = result.defaultTime;
          this.availableTo = result.availableTo;
          this.availableFrom = result.availableFrom;
        }
        switch (state) {
          case 0: {
            this.saveData();
            break;
          }
          case 1: {
            this.addService();
            this.isChange = true;
            break;
          }
          case 2: {
            this.addCategory(idService);
            this.isChange = true;
            break;
          }
          case 3: {
            this.addWorkInGroup(idService, idCategory, idGroupWork);
            this.isChange = true;
            break;
          }
          case 4: {
            this.addGroupWork(idService, idCategory);
            this.isChange = true;
            break;
          }
          case 5: {
            this.addWork(idService, idCategory);
            this.isChange = true;
            break;
          }
        }
        this.name = '';
      }
    });
  }

  findId(arr: IdModel[]): string {
    const idList = [];
    let id = '_0';
    arr.forEach(it => {
      idList.push(it.id);
    });
    for (let i = 1; i < arr.length + 2; i++) {
      if (!idList.includes('_' + i)) {
        id = '_' + i;
      }
    }
    return id;
  }

  save() {
    this.openDialog(0);
  }

  addServiceDialog() {
    this.openDialog(1);
  }

  addCategoryDialog(idService: string) {
    this.openDialog(2, idService);
  }

  addWorkDialog(idService: string, idCategory: string) {
    this.openDialog(5, idService, idCategory);
  }

  addGroupWorkDialog(idService: string, idCategory: string) {
    this.openDialog(4, idService, idCategory);
  }

  addWorkInGroupDialog(idService: string, idCategory: string, idGroupWork: string) {
    this.openDialog(3, idService, idCategory, idGroupWork);
  }

  private saveData() {
    this.priceListService.updateService(this.services, this.ownerEmailSend, result => {
      if (result) {
        this.isChange = false;
      }
    });
  }

  private addService() {
    const id = this.findId(this.services);
    if (id) {
      const newService = {
        id,
        name: this.name,
        categories: [],
        timestamp: '1000',
        email: ''
      };
      this.services.push(newService as Services);
    }
  }

  private addCategory(idService: string) {
    this.services.forEach(it => {
      if (it.id === idService) {
        const id = this.findId(it.categories);
        if (id) {
          const newCategory = {
            id,
            name: this.name,
            works: [],
            groupsWork: []
          };
          it.categories.push(newCategory as Category);
        }
      }
    });
  }

  private addGroupWork(idService: string, idCategory: string) {
    this.services.forEach(it => {
      if (it.id === idService) {
        it.categories.forEach(it1 => {
          if (it1.id === idCategory) {
            const id = this.findId(it1.groupsWork);
            if (id) {
              const newGroupWork = {
                id,
                name: this.name,
                works: [],
              };
              it1.groupsWork.push(newGroupWork as GroupWork);
            }
          }
        });
      }
    });
  }

  private addWorkInGroup(idService: string, idCategory: string, idGroupWork: string) {
    this.services.forEach(it => {
      if (it.id === idService) {
        it.categories.forEach(it1 => {
          if (it1.id === idCategory) {
            it1.groupsWork.forEach(it2 => {
              if (it2.id === idGroupWork) {
                const id = this.findId(it2.works);
                if (id) {
                  const newWork = {
                    id,
                    availableFrom: this.availableFrom,
                    availableTo: this.availableTo,
                    cost: this.price_max,
                    cost_work: this.price_min,
                    defaultTime: this.defaultTime,
                    name: this.name,
                    service: it.name
                  };
                  console.log(newWork);
                  it2.works.push(newWork as Work);
                }
              }
            });
          }
        });
      }
    });
  }

  private addWork(idService: string, idCategory: string) {
    this.services.forEach(it => {
      if (it.id === idService) {
        it.categories.forEach(it1 => {
          if (it1.id === idCategory) {
            const id = this.findId(it1.works);
            if (id) {
              const newWork = {
                id,
                availableFrom: this.availableFrom,
                availableTo: this.availableTo,
                cost: this.price_max,
                defaultTime: this.defaultTime,
                name: this.name,
                service: it.name
              };
              it1.works.push(newWork as Work);
            }
          }
        });
      }
    });
  }

  deleteServiceDialog(service: string) {
    this.openDialogDelete(0, service);
  }

  deleteCategoryDialog(service: string, category: string) {
    this.openDialogDelete(1, service, category);

  }

  deleteGroupWorkDialog(service: string, category: string, groupWork: string) {
    this.openDialogDelete(2, service, category, groupWork);
  }

  deleteWorkInGroupDialog(serviceId: string, categoryid: string, groupWorkId: string, workInGroupId: string) {
    this.openDialogDelete(3, serviceId, categoryid, groupWorkId, workInGroupId);
  }

  deleteWorkDialog(serviceId: string, categoryId: string, workId: string) {
    this.openDialogDelete(4, serviceId, categoryId, '', workId);
  }

  deleteService(service: string) {
    const newServices = [];
    this.services.forEach(it => {
      if (it.id !== service) {
        newServices.push(it);
      }
    });
    this.services = newServices;
  }

  deleteCategory(service: string, category: string) {
    const newCategory = [];
    this.services.forEach(it => {
      if (it.id === service) {
        it.categories.forEach(cat => {
          if (cat.id !== category) {
            newCategory.push(cat);
          }
        });
        it.categories = newCategory;
      }
    });
  }

  deleteGroupWork(service: string, category: string, groupWork: string) {
    const newGroupWork = [];
    this.services.forEach(it => {
      if (it.id === service) {
        it.categories.forEach(cat => {
          if (cat.id === category) {
            cat.groupsWork.forEach(group => {
              if (group.id !== groupWork) {
                newGroupWork.push(group);
              }
            });
            cat.groupsWork = newGroupWork;
          }
        });
      }
    });
  }

  private deleteWorkInGroup(service: string, category: string, groupWork: string, workInGroup: string) {
    const newWorks = [];
    this.services.forEach(it => {
      if (it.id === service) {
        it.categories.forEach(cat => {
          if (cat.id === category) {
            cat.groupsWork.forEach(group => {
              if (group.id === groupWork) {
                group.works.forEach(work => {
                  if (work.id !== workInGroup) {
                    newWorks.push(work);
                  }
                });
                group.works = newWorks;
              }
            });
          }
        });
      }
    });
  }

  private deleteWork(service: string, category: string, groupWork: string, workInGroup: string) {
    const newWorks = [];
    this.services.forEach(it => {
      if (it.id === service) {
        it.categories.forEach(cat => {
          if (cat.id === category) {
            cat.works.forEach(work => {
              if (work.id !== workInGroup) {
                newWorks.push(work);
              }
            });
            cat.works = newWorks;
          }
        });
      }
    });
  }

  private openDialogDelete(state1: number, service?: string, category?: string, groupWork?: string, workInGroup?: string) {
    const dialogRef = this.dialog.open(DialogDeleteDialogComponent, {
      width: '400px',
      data: {
        state1
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('state1 = ' + state1);
      if (result) {
        switch (state1) {
          case 0: {
            this.deleteService(service);
            this.isChange = true;
            break;
          }
          case 1: {
            this.deleteCategory(service, category);
            this.isChange = true;
            break;
          }
          case 2: {
            this.deleteGroupWork(service, category, groupWork);
            this.isChange = true;
            break;
          }
          case 3: {
            this.deleteWorkInGroup(service, category, groupWork, workInGroup);
            this.isChange = true;
            break;
          }
          case 4: {
            this.deleteWork(service, category, groupWork, workInGroup);
            this.isChange = true;
            break;
          }
        }
      }
    });
  }
}

interface DialogData {
  state: number;
  name: string;
  price_max: string;
  price_min: string;
  defaultTime: string;
  availableFrom: string;
  availableTo: string;
}

interface DialogData1 {
  state1: number;
}

@Component({
  templateUrl: 'dialog-add-dialog.html',
})
export class DialogAddDialogComponent {
  required = new FormControl('', [Validators.required]);
  required1 = new FormControl('', [Validators.required,
    Validators.pattern('[a-z]*|[A-Z]*|[А-Я]*|[а-я]*|[0-9]*'), Validators.maxLength(50)]);
  required2 = new FormControl('', [Validators.required,
    Validators.pattern('[0-9]*'), Validators.minLength(2)]);
  required22 = new FormControl('', [Validators.required,
    Validators.pattern('[0-9]*'), Validators.minLength(2)]);
  required3 = new FormControl('', [Validators.required,
    Validators.pattern('[0-9]*'), Validators.maxLength(4)]);
  required4 = new FormControl('', [Validators.pattern('[0-9]*'), Validators.maxLength(2)]);
  required5 = new FormControl('', [Validators.pattern('[0-9]*'), Validators.maxLength(2)]);


  constructor(
    public dialogRef: MatDialogRef<DialogAddDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: DialogData) {
  }

}

@Component({
  templateUrl: 'dialog-delete-dialog.html',
})
export class DialogDeleteDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<DialogAddDialogComponent>, @Inject(MAT_DIALOG_DATA) public data2: DialogData1) {
  }

}
