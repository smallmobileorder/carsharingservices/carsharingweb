import {Component, Inject, OnInit} from '@angular/core';
import {PersonalService} from '../../services/personal.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Approved, ProfileInfo, Status} from '../../models/Accounts';
import {FormControl} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatSnackBar} from '@angular/material';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.css']
})
export class UserInfoComponent implements OnInit {
  elements = [];
  private data: any;
  panelOpenState: boolean;
  profileInfo: ProfileInfo;
  toppings = new FormControl();
  private subscription: Subscription;
  toppingList: string[] = ['Некоректные данные по счету банка', 'Некорректный Email',
    'Некорректное фото водительского удостоверения на лицевой стороне', 'Некорректное фото водительского удостоверения на обратной стороне',
    'Некорректные данные водительского удостоверения', 'Некорректные данные паспорта', 'Некорректное главное фото паспорта',
    'Некорректное фото регистрации паспорта', 'Некорректное фото селфи с паспортом', 'Некорректно введена личная информация'];
  listKey = [false, false, false, false, false, false, false, false, false, false, false, false];
  listKey1: boolean[];
  private id: string;
  status = Status.WAITING;
  private approvedInfo: Approved;

  constructor(private personalService: PersonalService, private route: ActivatedRoute, public dialog: MatDialog,
              private snackBar: MatSnackBar, private router: Router) {
    this.subscription = route.params.subscribe(params => this.id = params.id);
    this.data = this.route.snapshot.data;
  }

  ngOnInit() {
    this.profileInfo = this.personalService.getRegistrationInfo(this.data);
    this.approvedInfo = this.personalService.getApprovedInfo(this.data);
    this.status = this.personalService.getStatus(this.approvedInfo);
    const ai = this.approvedInfo;
    this.listKey1 = [(ai.bad_bank_data || ai.bad_card_data), ai.bad_email_data,
      ai.bad_license_front_photo, ai.bad_license_back_photo, ai.bad_license_data, ai.bad_passport_data, ai.bad_passport_first_photo,
      ai.bad_passport_reg_photo, ai.bad_passport_selfie, ai.bad_personal_data];
    const values = [];
    let i: number;
    for (i = 0; i < this.listKey1.length; i++) {
      if (this.listKey1[i]) {
        values.push(this.toppingList[i]);
      }
    }
    this.toppings.setValue(values);
  }

  openDialog(state: number) {
    const dialogRef = this.dialog.open(DialogApproveDialogComponent, {
      width: '400px',
      data: {state}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (state === 1) {
          this.personalService.setApproved(this.id, this.listKey, true, res => {
            if (res) {
              this.status = Status.ACCEPTED;
              // this.snackBar.open('Заявка одобрена');

            } else {
              this.snackBar.open('Ошибка :(');
            }
          });
        } else {
          for (const i of this.toppings.value) {
            const index = this.toppingList.indexOf(i);
            this.listKey[index] = true;
          }
          this.personalService.setApproved(this.id, this.listKey, false, res => {
            if (res) {
              this.status = Status.CANCELED;
              // this.snackBar.open('Заявка отклонена на доработку');
            } else {
              this.snackBar.open('Ошибка :(');
            }
          });
        }
      }
    });
  }


  reject() {
    this.openDialog(0);
  }

  accept() {
    this.openDialog(1);
  }

}

interface DialogData {
  state: number;
}


@Component({
  templateUrl: 'dialog-approve-dialog.html',
})
export class DialogApproveDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<DialogApproveDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: DialogData) {
  }

  onNoClick(b: boolean) {
    this.dialogRef.close(b);
  }

  onClearClick(b: boolean) {
    this.dialogRef.close(b);
  }
}

