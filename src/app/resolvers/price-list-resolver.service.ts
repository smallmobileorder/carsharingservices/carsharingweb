import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {forkJoin, Observable} from 'rxjs';
import {PriceListService} from '../services/price-list.service';

@Injectable({
  providedIn: 'root'
})
export class PriceListResolverService implements Resolve<any> {

  constructor(private priceListService: PriceListService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return forkJoin(this.priceListService.getServices(), this.priceListService.getOwner());
  }
}

