import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {EMPTY, forkJoin, Observable} from 'rxjs';
import {AnalyticsService} from '../services/analytics.service';
import {PersonalService} from '../services/personal.service';

@Injectable({
  providedIn: 'root'
})
export class OrderInfoResolverService implements Resolve<any> {

  constructor(private analyticsService: AnalyticsService, private personalService: PersonalService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    const idUser = route.paramMap.get('idUser');
    const idOrder = route.paramMap.get('idOrder');
    return forkJoin(this.analyticsService.getOrderById(idUser, idOrder), this.personalService.getProfileById(idUser));
  }
}

