import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {EMPTY, forkJoin, Observable} from 'rxjs';
import {PersonalService} from '../services/personal.service';

@Injectable({
  providedIn: 'root'
})
export class PersonalInfoResolverService implements Resolve<any> {

  constructor(private personalService: PersonalService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    const id = route.paramMap.get('id');
    return forkJoin(this.personalService.getRegistrationById(id), this.personalService.getApprovedById(id));
  }
}

