import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {PriceListService} from '../services/price-list.service';
import {AnalyticsService} from '../services/analytics.service';

@Injectable({
  providedIn: 'root'
})
export class AnalyticsResolverService implements Resolve<any> {

  constructor(private analyticsService: AnalyticsService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return this.analyticsService.getAllOrders();
  }
}

