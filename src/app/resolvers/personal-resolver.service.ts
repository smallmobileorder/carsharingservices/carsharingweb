import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {EMPTY, forkJoin, Observable} from 'rxjs';
import {PersonalService} from '../services/personal.service';

@Injectable({
  providedIn: 'root'
})
export class PersonalResolverService implements Resolve<any> {

  constructor(private personalService: PersonalService, private router: Router) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return forkJoin(this.personalService.getProfiles(), this.personalService.getApproved());
  }
}

