import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {forkJoin, Observable} from 'rxjs';
import {PriceListService} from '../services/price-list.service';
import {AnalyticsService} from '../services/analytics.service';
import {ResponcePersonalService} from '../services/responce-personal.service';
import {PersonalService} from '../services/personal.service';

@Injectable({
  providedIn: 'root'
})
export class ResponcePersonalResolverService implements Resolve<any> {

  constructor(private responcePersonalService: ResponcePersonalService, private personalService: PersonalService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return forkJoin(this.personalService.getProfiles(), this.responcePersonalService.getResponces());
  }
}

