import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {AppRoutingModule} from './app.router';
import {AppComponent} from './app.component';
import {LoginComponent} from './components/login/login.component';
import {HttpClientModule} from '@angular/common/http';
import {HeaderComponent} from './components/header/header.component';
import {BarChartComponent} from './components/barChart/barChart.component';
import {PriceComponent} from './components/price/price.component';
import {PersonalComponent} from './components/personal/personal.component';
import {ResponsePersonalComponent} from './components/response-personal/response-personal.component';
import {MainComponent} from './components/main/main.component';
import {StatisticComponent} from './components/statistic/statistic.component';
import {CommonStatisticComponent} from './components/common-statistic/common-statistic.component';
import {FinancialStatisticComponent} from './components/financial-statistic/financial-statistic.component';
import {OrdersComponent} from './components/orders/orders.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AuthService} from './services/auth.service';
import {AngularFireModule} from 'angularfire2';
import {environment} from '../environments/environment';
import {AngularFireAuthModule} from 'angularfire2/auth';
import {AngularFireDatabaseModule} from 'angularfire2/database';
import {AngularFireStorageModule} from 'angularfire2/storage';
import {AnalyticsComponent} from './components/analytics/analytics.component';
import {
  MatPaginatorModule,
  MatSliderModule,
  MatTableModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatSelectModule, MatButtonModule, MatDialogModule, MatSnackBarModule, MatCardModule, MatInputModule, MatIconModule, MatGridListModule
} from '@angular/material';
import {DialogApproveDialogComponent, UserInfoComponent} from './components/user-info/user-info.component';
import {DialogAddDialogComponent, DialogDeleteDialogComponent, PriceListComponent} from './components/price-list/price-list.component';
import { OrderInfoComponent } from './components/order-info/order-info.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainComponent,
    HeaderComponent,
    BarChartComponent,
    PriceComponent,
    PersonalComponent,
    ResponsePersonalComponent,
    StatisticComponent,
    CommonStatisticComponent,
    FinancialStatisticComponent,
    OrdersComponent,
    AnalyticsComponent,
    UserInfoComponent,
    DialogApproveDialogComponent,
    DialogAddDialogComponent,
    DialogDeleteDialogComponent,
    PriceListComponent,
    OrderInfoComponent],
  imports: [
    MatSliderModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    AngularFireStorageModule,
    MatTableModule,
    MatPaginatorModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatSelectModule,
    MatButtonModule,
    MatDialogModule,
    MatSnackBarModule,
    MatCardModule,
    MatInputModule,
    MatIconModule,
    MatGridListModule
  ],
  entryComponents: [DialogApproveDialogComponent, DialogAddDialogComponent, DialogDeleteDialogComponent],
  providers: [
    AuthService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
