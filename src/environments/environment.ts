// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyD7M-HtOlaY7xkBUQtL3Zq4xaLtW8thYaw',
    authDomain: 'carsharingservice-5fb53.firebaseapp.com',
    databaseURL: 'https://carsharingservice-5fb53.firebaseio.com',
    projectId: 'carsharingservice-5fb53',
    storageBucket: 'carsharingservice-5fb53.appspot.com',
    messagingSenderId: '877980284604',
    appId: '1:877980284604:web:63babb7713f7c4e7a302b9',
    measurementId: 'G-2QKCXNB1GM'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
